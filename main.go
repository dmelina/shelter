package main

import (
	"fmt"
	"os"

	"github.com/Sirupsen/logrus"
	"github.com/codegangsta/cli"
)

// Options represents the command/config options.
type Options struct {
	PrivateToken string
	GitlabURL    string
	Debug        bool
	Config       string
}

var (
	// Name represents the package name
	Name = "shelter"

	logger = logrus.New()
)

// NewOptions constructor
func NewOptions(c *cli.Context) (*Options, error) {
	debug := c.GlobalBool("debug")
	if debug {
		logger.Level = logrus.DebugLevel
	}

	gitlabURL := c.GlobalString("gitlab-url")

	privateToken := c.GlobalString("private-token")
	if privateToken == "" {
		return nil, fmt.Errorf("No Private token found, please set GITLAB_PRIVATE_TOKEN or --private-token")
	}
	config := c.GlobalString("config")
	//if config == "" {
	//  return nil, fmt.Errorf("No Config file token found, please set SHELTER_CONFIG or --config")
	//}

	return &Options{
		GitlabURL:    gitlabURL,
		PrivateToken: privateToken,
		Debug:        debug,
		Config:       config,
	}, nil
}

func main() {
	app := NewApp()

	app.Before = func(c *cli.Context) error {

		//config, err := LoadConfig(c.GlobalString("config"))
		//if err != nil {
		//  logger.Fatalf("Config: %s", err)
		//}

		//opts.Config = config
		//logger.Debug(fmt.Sprintf("Options: %#v", opts))
		return nil
	}

	// Main action
	//app.Action = func(c *cli.Context) {
	//  fmt.Printf("ARGS: %#v\n", c.Args())
	//}

	app.Run(os.Args)
}
