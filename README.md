# Shelter - Builds manager for Gitlab

/!\ Work in progress

## Screenshot

FIXME

## Building shelter

`shelter` is built using Go version 1.5 or greater.

Next, you'll need `glide` to install the golang dependencies. You can do this by running
```
go get github.com/Masterminds/glide
```

In Go 1.5 you'll need the vendor experiment enabled, so make sure to export
`GO15VENDOREXPERIMENT=1` in your shell (see [Go 1.5 Vendor Experiment][1])

In your git checkout ($GOPATH/src/github.com/wercker/wercker), run:
```
glide install --quick
```

This command should download the appropiate dependencies.

Build `shelter` by running
```
go build
```

## Before you start

Get your personal private token from [Gitlab][2]

Add to your env: `GITLAB_PRIVATE_TOKEN` and `GITLAB_URL` (aka https://gitlab.com)

## Quickstart

FIXME

[1] https://docs.google.com/document/d/1Bz5-UB7g2uPBdOx-rw5t9MxJwkfpx90cqG9AFL0JAYo/edit 
[2] https://gitlab.com/profile/account
