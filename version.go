package main

import "fmt"

var (
	// Version represents the package version (git tag)
	Version = "unknown"
	// Revision represents the last git commit
	Revision = ""
)

// FullVersion returns the full package version
func FullVersion(v string, r string) string {
	if r != "" {
		r = fmt.Sprintf(" (commit: %s)", r)
	}
	return fmt.Sprintf("%s%s", v, r)
}
