package main

import (
	"fmt"
	"os"
	"sort"
	"time"

	"github.com/codegangsta/cli"
	"github.com/nsf/termbox-go"
)

// TopBuildWindow represents the builds screen
type TopBuildWindow struct {
	Gitlab *Gitlab
	Target string
	Alert  string

	// Sub- Windows
	Header     Window
	Build      Window
	AlertModal Window
}

// NewTopBuildWindow constructor
func NewTopBuildWindow(gitlab *Gitlab, target string) *TopBuildWindow {
	return &TopBuildWindow{
		Gitlab: gitlab,
		Target: target,
	}
}

// Init build screen and all subwindows
func (w *TopBuildWindow) Init() error {

	w.Header = NewHeaderWindow(w)
	w.Build = NewBuildWindow(w)
	w.AlertModal = NewAlertWindow(w)

	for _, win := range []Window{
		w.Header,
		w.Build,
		w.AlertModal,
	} {
		if err := win.Init(); err != nil {
			return err
		}
	}
	return nil
}

// Draw all the subwindows
func (w *TopBuildWindow) Draw(x, y, width, height int) {
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	w.Header.Draw(x, y, width, height)
	w.Build.Draw(x, y, width, height)
	w.AlertModal.Draw(x, y, width, height)
}

// Redraw all the subwindows
func (w *TopBuildWindow) Redraw() {
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	width, height := termbox.Size()
	w.Draw(0, 0, width, height)
	termbox.Flush()
}

// HandleEvent get events
func (w *TopBuildWindow) HandleEvent(ev termbox.Event) error {
	logger.Debugf("Catch event Key: %#v", ev)
	w.Build.HandleEvent(ev)
	return nil
}

// HeaderWindow shows the title bar
type HeaderWindow struct {
	*SubWindow
}

// NewHeaderWindow constructor
func NewHeaderWindow(w *TopBuildWindow) *HeaderWindow {
	return &HeaderWindow{&SubWindow{w}}
}

// Init build screen and all subwindows
func (w *HeaderWindow) Init() error {
	return nil
}

// Draw header window
func (w *HeaderWindow) Draw(x, y, width, height int) {
	title := fmt.Sprintf("*shelter* repo: %s", w.Target)
	tbSet(x, y, termbox.ColorDefault, termbox.ColorDefault, title)
}

// HandleEvent get events
func (w *HeaderWindow) HandleEvent(ev termbox.Event) error {
	return nil
}

// AlertWindow shows the alert
type AlertWindow struct {
	*SubWindow
}

// NewAlertWindow constructor
func NewAlertWindow(w *TopBuildWindow) *AlertWindow {
	return &AlertWindow{&SubWindow{w}}
}

// Init alert screen
func (w *AlertWindow) Init() error {
	return nil
}

// Draw alert window
func (w *AlertWindow) Draw(x, y, width, height int) {
	if w.Alert == "" {
		return
	}
	tbSet(0, height-1, termbox.ColorDefault, termbox.ColorDefault, w.Alert)
}

// BuildWindow shows the builds list
type BuildWindow struct {
	*SubWindow
}

// NewBuildWindow constructor
func NewBuildWindow(w *TopBuildWindow) *BuildWindow {
	return &BuildWindow{&SubWindow{w}}
}

// Init build screen and all subwindows
func (w *BuildWindow) Init() error {
	w.refresh()
	return nil
}

func (w *BuildWindow) refresh() {
	w.Alert = "Get builds..."
	w.Redraw()

	logger.Debugf("Get project info for: %s", w.Target)
	project, err := w.Gitlab.GetProject(w.Target)
	if err != nil {
		logger.Fatal(err)
	}
	w.Gitlab.Project = project

	_, height := termbox.Size()
	numRows := height - 4

	logger.Debugf("Get builds for project (name: %s id: %d)", w.Gitlab.Project.Name, w.Gitlab.Project.ID)

	w.Gitlab.Project.Builds, err = w.Gitlab.GetBuilds(w.Gitlab.Project.ID, numRows)
	if err != nil {
		logger.Println(err)
	}
	w.Alert = ""
	w.Redraw()
}

// Draw all the builds
func (w *BuildWindow) Draw(x, y, width, height int) {
	title := fmt.Sprintf("%-10s%-10s%-10s%-10s%-10s%-10s%-10s%s", "status", "id", "commit", "ref", "stage", "name", "duration", "finished")
	tbSet(x+1, y+2, termbox.ColorDefault|termbox.AttrUnderline, termbox.ColorDefault, title)

	builds := w.Gitlab.Project.Builds

	if builds != nil {
		sort.Sort(ByFinishedAt(builds))
	}

	for i, build := range builds {
		var finished, duration string
		if build.Status != "running" {
			finished = fmt.Sprintf(build.FinishedAt.Format(time.RFC822))
			duration = fmt.Sprintf("%.0f sec", build.FinishedAt.Sub(build.StartedAt).Seconds())
		}
		if build.Status == "running" {
			duration = fmt.Sprintf("%.0f sec", time.Now().Sub(build.StartedAt).Seconds())
		}
		tbSet(1, i+3, termbox.ColorWhite, termbox.ColorDefault, fmt.Sprintf("%-10s%-10d%-10s%-10s%-10s%-10s%-10s%s",
			build.Status,
			build.ID,
			build.Commit[0:7],
			build.Ref,
			build.Stage,
			build.Name,
			duration,
			finished,
		))
	}
}

// HandleEvent get events
func (w *BuildWindow) HandleEvent(ev termbox.Event) error {
	switch ev.Type {
	case termbox.EventKey:
		switch ev.Key {
		case termbox.KeyCtrlR:
			w.refresh()
			return nil
		}
	}
	return nil
}

//

var (
	uiCommand = cli.Command{
		Name:  "ui",
		Usage: "Run shelter UI",
		Action: func(c *cli.Context) {
			//TODO @dmelina implement soft exit to avoid breaking the teminal
			f, err := os.Create("shelter.log")
			if err != nil {
				logger.Fatal(err)
			}
			logger.Out = f
			defer f.Close()

			opts, err := NewOptions(c)
			if err != nil {
				logger.Fatal(err)
			}
			gitlab := NewGitlab(opts)
			target := c.Args().First()

			if err := termbox.Init(); err != nil {
				logger.Fatal(err)
			}
			defer termbox.Close()

			buildWindow := NewTopBuildWindow(gitlab, target)
			buildWindow.Init()

		mainloop:
			for {
				switch ev := termbox.PollEvent(); ev.Type {
				case termbox.EventKey:
					switch ev.Key {
					case termbox.KeyCtrlC:
						break mainloop
					default:
						buildWindow.HandleEvent(ev)
						buildWindow.Redraw()
					}
				case termbox.EventResize:
					buildWindow.Redraw()
				}
			}
		},
	}
)
