package main

import "github.com/nsf/termbox-go"

// Window represent window interface for drawing things on screen
type Window interface {
	Init() error
	Draw(x, y, width, height int)
	HandleEvent(ev termbox.Event) error
}

// SubWindow implement the base Window
type SubWindow struct {
	*TopBuildWindow
}

// Init noop
func (w *SubWindow) Init() error {
	return nil
}

// Draw noop
func (w *SubWindow) Draw(x, y, width, height int) {
}

// HandleEvent noop
func (w *SubWindow) HandleEvent(ev termbox.Event) error {
	return nil
}

// tbSet change cell of the terminal back buffer
func tbSet(x, y int, fg, bg termbox.Attribute, msg string) {
	for _, c := range msg {
		foreground := fg
		if c == ' ' {
			foreground = termbox.ColorDefault
		}
		termbox.SetCell(x, y, c, foreground, bg)
		x++
	}
}
