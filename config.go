package main

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

// Projects represents git project list.
type Projects []string

// Config represents configuration file.
type Config struct {
	Projects Projects
}

// LoadConfig return config struct pointer.
//
func LoadConfig(file string) (*Config, error) {

	var config Config
	f, err := os.Open(file)
	defer f.Close()
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}

	return &config, nil
}
