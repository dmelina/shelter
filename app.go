package main

import (
	"fmt"

	"github.com/codegangsta/cli"
)

const (
	// Usage reprensents the command line usage.
	Usage = "Builds manager for Gitlab"
)

// NewApp represent the codegangsta CLI
func NewApp() *cli.App {
	app := cli.NewApp()
	app.Name = Name
	app.Usage = Usage
	app.Version = FullVersion(Version, Revision)
	app.Authors = []cli.Author{
		cli.Author{Name: "Davy MELINA", Email: "davy.melina@gmail.com"},
	}
	app.Flags = GlobalFlagSet
	app.Before = func(c *cli.Context) error {
		return nil
	}
	app.CommandNotFound = func(c *cli.Context, command string) {
		fmt.Printf("Command not found: wtf\n")
	}
	app.Commands = []cli.Command{
		uiCommand,
	}
	return app
}
