package main

import "testing"

func TestFullVersion(t *testing.T) {
	expectedVersion := "1.0 (commit: 1234567)"
	version := FullVersion("1.0", "1234567")
	if version != expectedVersion {
		t.Errorf("Expected version: %s found %s", expectedVersion, version)
	}
}
