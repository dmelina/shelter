package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
)

// This is the directory where our test fixtures are
const fixtureDir = "./fixtures"

func testOpenFixture(t *testing.T, name string) []byte {
	f, err := os.Open(name)
	defer f.Close()
	if err != nil {
		t.Fatal(err)
	}
	data, err := ioutil.ReadAll(f)
	if err != nil {
		t.Fatal(err)
	}
	return data
}

func TestGetProject(t *testing.T) {
	target := "diaspora/diaspora-project-site"
	data := testOpenFixture(t, filepath.Join(fixtureDir, "project.json"))

	handler := func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, fmt.Sprintf("%s", data))
	}
	server := httptest.NewServer(http.HandlerFunc(handler))
	defer server.Close()

	opts := &Options{
		GitlabURL: server.URL,
	}
	gitlab := NewGitlab(opts)
	project, err := gitlab.GetProject(target)
	if err != nil {
		t.Error(err)
	}
	if project.Name != target {
		t.Error(err)
	}
}

func TestGetBuilds(t *testing.T) {
	data := testOpenFixture(t, filepath.Join(fixtureDir, "builds.json"))

	handler := func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, fmt.Sprintf("%s", data))
	}
	server := httptest.NewServer(http.HandlerFunc(handler))
	defer server.Close()

	opts := &Options{
		GitlabURL: server.URL,
	}
	gitlab := NewGitlab(opts)
	builds, err := gitlab.GetBuilds(1, 20)
	if err != nil {
		t.Error(err)
	}
	if len(builds) != 2 {
		t.Errorf("Expected 2 builds, found %d", len(builds))
	}
}
