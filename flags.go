package main

import "github.com/codegangsta/cli"

// Flags for settings the CLI
var (
	// GlobalFlagSet represents the global flags
	GlobalFlagSet = []cli.Flag{
		cli.StringFlag{Name: "private-token", Usage: "Set private token", Value: "", EnvVar: "GITLAB_PRIVATE_TOKEN"},
		cli.StringFlag{Name: "gitlab-url", Usage: "Set Gitlab URL", Value: "https://gitlab.com", EnvVar: "GITLAB_URL"},
		cli.StringFlag{Name: "config", Usage: "Config file", Value: "", EnvVar: "SHELTER_CONFIG"},
		cli.BoolFlag{Name: "debug", Usage: "Enable debug mode"},
	}
)
