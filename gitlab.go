package main

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/antonholmquist/jason"
)

// Gitlab represents the API client for Gitlab
type Gitlab struct {
	PrivateToken string
	URL          string
	Debug        bool
	Project      *Project
	Client       *http.Client
}

// Project is the Gitlab project
type Project struct {
	Name   string
	ID     int64
	Builds Builds
}

// Build is the Gitlab build
type Build struct {
	Status     string
	ID         int64
	Commit     string
	Ref        string
	Stage      string
	Name       string
	StartedAt  time.Time
	FinishedAt time.Time
}

// Builds is Gitlab build list
type Builds []*Build

// ByFinishedAt represent sort build by finished date desc
type ByFinishedAt []*Build

// Len for sortable
func (s ByFinishedAt) Len() int {
	return len(s)
}

// Swap for sortable
func (s ByFinishedAt) Less(i, j int) bool {
	return s[i].FinishedAt.After(s[j].FinishedAt)
}

// Swap for sortable
func (s ByFinishedAt) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// NewGitlab is Gitlab constructor
func NewGitlab(opts *Options) *Gitlab {
	return &Gitlab{
		PrivateToken: opts.PrivateToken,
		URL:          opts.GitlabURL,
		Debug:        opts.Debug,
		Client:       &http.Client{},
		Project:      &Project{Builds: Builds{}},
	}
}

// GetProject return the Gitlab project
func (g *Gitlab) GetProject(target string) (*Project, error) {

	url := fmt.Sprintf("%s/api/v3/projects/%s", g.URL, url.QueryEscape(target))

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("PRIVATE-TOKEN", g.PrivateToken)

	resp, err := g.Client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Project not found")
	}

	v, err := jason.NewObjectFromReader(resp.Body)
	if err != nil {
		return nil, err
	}
	id, err := v.GetInt64("id")
	if err != nil {
		return nil, err
	}
	return &Project{
		Name: target,
		ID:   id,
	}, nil
}

// GetBuilds return builds for a project id and number of rows
func (g *Gitlab) GetBuilds(p int64, n int) ([]*Build, error) {

	url := fmt.Sprintf("%s/api/v3/projects/%d/builds?per_page=%d", g.URL, p, n)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("PRIVATE-TOKEN", g.PrivateToken)

	resp, err := g.Client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("No build not found")
	}

	v, err := jason.NewValueFromReader(resp.Body)
	if err != nil {
		return nil, err
	}

	builds, err := v.Array()
	if err != nil {
		return nil, err
	}

	var result []*Build
	for _, b := range builds {
		data, err := b.Object()
		if err != nil {
			logger.Error(err)
			continue
		}
		status, err := data.GetString("status")
		if err != nil {
			logger.Error(err)
			continue
		}
		name, err := data.GetString("name")
		if err != nil {
			logger.Error(err)
			continue
		}
		id, err := data.GetInt64("id")
		if err != nil {
			logger.Error(err)
			continue
		}
		ref, err := data.GetString("ref")
		if err != nil {
			logger.Error(err)
			continue
		}
		stage, err := data.GetString("stage")
		if err != nil {
			logger.Error(err)
			continue
		}
		commit, err := data.GetString("commit", "id")
		if err != nil {
			logger.Error(err)
			continue
		}
		started, err := data.GetString("started_at")
		if err != nil {
			logger.Error(err)
			continue
		}
		startedAt, err := time.Parse("2006-01-02T15:04:05.999Z", started)
		if err != nil {
			logger.Error(err)
			continue
		}
		build := &Build{
			Status:    status,
			Name:      name,
			ID:        id,
			Ref:       ref,
			Stage:     stage,
			Commit:    commit,
			StartedAt: startedAt,
		}
		if status == "pending" || status == "running" {
			result = append(result, build)
			continue
		}
		finished, err := data.GetString("finished_at")
		if err != nil {
			logger.Error(err)
			continue
		}
		finishedAt, err := time.Parse("2006-01-02T15:04:05.999Z", finished)
		if err != nil {
			logger.Error(err)
			continue
		}
		build.FinishedAt = finishedAt
		result = append(result, build)
	}
	return result, nil
}
