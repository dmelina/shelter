.PHONY: version verify test vet lint deps build docker push-docker

# Info
NAME=shelter
VERSION := $(shell git describe --abbrev=0 --tags 2> /dev/null || echo unknown)
REVISION := $(shell git rev-parse --short HEAD || echo unknown)

# Docker
DOCKER_HOST=172.17.0.1:4243
DOCKER_VERSION=1.10.3

# GO
GO_LDFLAGS ?= "-X main.Name=$(NAME) -X main.Version=$(VERSION)"
GO_LDFLAGS_DEV ?= "-X main.Name=$(NAME) -X main.Version=$(VERSION) -X main.Revision=$(REVISION)"
BUILD_PLATFORMS ?= "linux/amd64"
export GO15VENDOREXPERIMENT := 1

help:
	# make version - show information about current version
	# make verify - run vet, lint and test
	# make deps - install dependencies
	# make build - build artifacts
	# make docker - build docker container
	# make build-develop - build artifacts (staging)
	# make docker-develop - build docker container (staging)
version:
	@echo Name: $(NAME)
	@echo Version: $(VERSION)
	@echo Revision: $(REVISION)

verify: vet lint test

test:
	@go test -v $(glide novendor)

vet:
	@go vet *.go

lint:
	@golint *.go

deps:
	@go get -u github.com/golang/lint/golint
	@go get -u github.com/mitchellh/gox
	@go get -u github.com/Masterminds/glide
	@glide install

#ifeq (, $(shell docker version))
#	# Install docker
#	@curl -fsSL -o /usr/local/bin/docker https://get.docker.com/builds/Linux/x86_64/docker-$(DOCKER_VERSION)
#	@chmod +x /usr/local/bin/docker
#endif

build:
	#TODO: Need to improve and remove gitlab.com for gitlab-ci
	gox -osarch=$(BUILD_PLATFORMS) \
    	-ldflags=$(GO_LDFLAGS) \
		-output="builds/$(NAME)-{{.OS}}-{{.Arch}}"

docker:
	@DOCKER_HOST=$(DOCKER_HOST) docker build -t $(NAME):$(VERSION) .

push-docker:

build-develop:
	gox -osarch=$(BUILD_PLATFORMS) \
    	-ldflags=$(GO_LDFLAGS_DEV) \
		-output="builds/$(NAME)-{{.OS}}-{{.Arch}}"

docker-develop:
	@DOCKER_HOST=$(DOCKER_HOST) docker build -t $(NAME):$(REVISION) .

push-docker-develop:

#proto:
#	protoc --go_out=. sandbox.proto
